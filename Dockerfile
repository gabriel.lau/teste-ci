#
#--------------------------------------------------------------------------
# Project container
#--------------------------------------------------------------------------
#
#
ARG PYTHON_VERSION=3.7-slim
ARG CI_REGISTRY
ARG REGISTRY_WORKSPACE
ARG IMAGE_BACKEND
ARG IMAGE_FRONTEND
ARG IMAGE_BACKEND_DEPENDENCIES
ARG BACKEND_VERSION="latest"
ARG FRONTEND_VERSION="latest"

FROM ${CI_REGISTRY}/${REGISTRY_WORKSPACE}/${IMAGE_BACKEND}:${BACKEND_VERSION} AS backend
FROM ${CI_REGISTRY}/${REGISTRY_WORKSPACE}/${IMAGE_BACKEND_DEPENDENCIES}:${BACKEND_VERSION} AS backend_dependencies
FROM ${CI_REGISTRY}/${REGISTRY_WORKSPACE}/${IMAGE_FRONTEND}:${FRONTEND_VERSION} AS frontend
FROM python:${PYTHON_VERSION}

ENV PYTHONUNBUFFERED 1

# Install packages needed to run your application (not build deps):
#   mime-support -- for mime types when serving static files
# We need to recreate the /usr/share/man/man{1..8} directories first because
# they were clobbered by a parent image.
RUN set -ex \
    && RUN_DEPS=" \
        libpcre3 \
        mime-support \
    " \
    && seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{} \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

# Install Python venv dependencies from another image
COPY --from=backend_dependencies /cache/venv /cache/venv

# Copy in your requirements file
COPY --from=backend /backend/requirements.txt /requirements.txt

ENV PATH="/cache/venv/bin:$PATH"

# Copy your application code to the container (make sure you create a .dockerignore file if any large files or directories should be excluded)
RUN mkdir /project/
WORKDIR /project/

###
### Fetch backend source-code
###
COPY --from=backend /backend/ /project/

###
### Fetch frontend build files
###
COPY --from=frontend /frontend/ /project/static/

EXPOSE 8000
